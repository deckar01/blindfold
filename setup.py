from distutils.core import setup
setup(
    name='blindfold',
    packages=['blindfold'],
    version='0.0.1',
    description='A declarative response filter for python APIs',
    author='Jared Deckard',
    author_email='jared@shademaps.com',
    url='https://gitlab.com/deckar01/blindfold',
    download_url='https://gitlab.com/deckar01/blindfold/repository/0.0.1/archive.tar.gz',
    keywords=['filter', 'view', 'content', 'secure', 'default', 'flask', 'marshmallow'],
    classifiers=[],
)
