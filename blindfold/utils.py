def is_iterable(data):
    if data is None:
        return False
    return hasattr(data, '__iter__')
