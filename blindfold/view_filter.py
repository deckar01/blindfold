import json
from functools import wraps
from marshmallow import Schema, fields
from blindfold.blindfold import Blindfold
from blindfold.errors import Errors
from blindfold.utils import is_iterable


class ViewFilter(Schema):
    @staticmethod
    def visibility(record):
        return False


class ViewFilterMixin(object):
    def has_owner(self, user):
        return False

    def has_member(self, user):
        return False

    def get_filter(self, viewer):
        if not viewer:
            if not hasattr(self, 'PublicViewFilter'):
                raise Errors.NotAllowed('Permission denied.')
            return self.PublicViewFilter
        if viewer.is_admin():
            return self.AdminViewFilter
        if self.has_owner(viewer):
            return self.OwnerViewFilter
        if self.has_member(viewer):
            return self.MemberViewFilter
        if not hasattr(self, 'InternalViewFilter'):
            raise Errors.NotAllowed('Permission denied.')
        return self.InternalViewFilter

    def filter(self, viewer):
        model_filter = self.get_filter(viewer)
        visible = model_filter.visibility(self)
        if not visible:
            raise Errors.NotFound('{} not found.'.format(self.__class__.__name__))
        self.filter_fields(model_filter, viewer)
        data, _ = model_filter().dump(self)
        return data

    def filter_fields(self, model_filter, viewer):
        for field_name, field in model_filter._declared_fields.items():
            if isinstance(field, fields.Nested) and issubclass(field.nested, ViewFilter):
                field_value = getattr(self, field_name, None)
                if is_iterable(field_value):
                    field_values = list(filter(field.nested.visibility, field_value))
                    setattr(self, field_name, field_values)
                    for field_value in field_values:
                        field_value.filter_fields(field.nested, viewer)
                else:
                    field_visible = field_value and field.nested.visibility(field_value)
                    if not field_visible:
                        setattr(self, field_name, None)
                    else:
                        field_value.filter_fields(field.nested, viewer)

    @classmethod
    def filter_list(Model, records, viewer):
        model_filter = Model.get_list_filter(viewer)
        records = list(filter(model_filter.visibility, records))
        for record in records:
            record.filter_fields(model_filter, viewer)

        # HACK: Work around https://github.com/marshmallow-code/marshmallow/issues/315
        # This allows the post_dump view filter to be reused for lists.
        class ModelListFilter(Schema):
            items = fields.List(fields.Nested(model_filter))
        data, _ = ModelListFilter().dump({'items': records})
        return data['items']

    @classmethod
    def get_list_filter(Model, viewer):
        if not viewer:
            if not getattr(Model, 'PublicListViewFilter', None):
                raise Errors.NotAllowed('Permission denied.')
            return Model.PublicListViewFilter
        if viewer.is_admin():
            return Model.AdminListViewFilter
        if not getattr(Model, 'InternalViewFilter', None):
            raise Errors.NotAllowed('Permission denied.')
        return Model.InternalListViewFilter

    @classmethod
    def filter_response(Model, many=False):
        def decorator(fn):
            @wraps(fn)
            def wrapper(*args, **kwargs):
                result = fn(*args, **kwargs)
                viewer = Blindfold.get_viewer()
                if many:
                    data = Model.filter_list(result, viewer)
                else:
                    data = result.filter(viewer)
                return json.dumps(data)
            return wrapper
        return decorator
